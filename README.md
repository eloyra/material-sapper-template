# material-sapper-template

This is an opinionated template project for building [Sapper][sapper_link] applications with [SMUI][smui_link] and component-level 
SCSS integration.

SMUI integration's based on [this][smui_integration_template] template and [this][smui_integration_example] example.
SCSS integration's managed with [Svelte Preprocess][svelte_preprocess_link] (so all other features leveraged by that 
preprocessor are also available though not necessarily configured).

I will document more details in the future.

<!--- LINKS --->
[sapper_link]: https://sapper.svelte.dev/
[smui_link]: https://github.com/hperrin/svelte-material-ui
[smui_integration_template]: https://github.com/kafai97/sapper-smui-boilerplate
[smui_integration_example]: https://github.com/hperrin/smui-example-rollup
[svelte_preprocess_link]: https://github.com/kaisermann/svelte-preprocess
